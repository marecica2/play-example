package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import play.data.validation.MaxSize;
import play.db.jpa.Model;

@Entity
@Table(name = "example.Post")
public class Post extends Model
{

    public String title;
    public Date postedAt;

    @Lob
    @MaxSize(10000)
    @Type(type = "org.hibernate.type.TextType")
    public String content;

    @ManyToOne
    public User author;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    public List<Comment> comments;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "example.POST_TAG", joinColumns = { @JoinColumn(name = "POST_ID") }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID") })
    public Set<Tag> tags;

    public Post(User author, String title, String content)
    {
        this.comments = new ArrayList<Comment>();
        this.tags = new TreeSet<Tag>();
        this.author = author;
        this.title = title;
        this.content = content;
        this.postedAt = new Date();
    }

    public Post previous()
    {
        return Post.find("postedAt < ? order by postedAt desc", postedAt).first();
    }

    public Post next()
    {
        return Post.find("postedAt > ? order by postedAt asc", postedAt).first();
    }

    public Post addComment(String author, String content)
    {
        Comment newComment = new Comment(this, author, content).save();
        this.comments.add(newComment);
        this.save();
        return this;
    }

    public Post tagItWith(String name)
    {
        tags.add(Tag.findOrCreateByName(name));
        return this;
    }

    public static List<Post> findTaggedWith(String tag)
    {
        return Post.find("select distinct p from Post p join p.tags as t where t.name = ?", tag).fetch();
    }

    @Override
    public String toString()
    {
        return title;
    }

}