package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import models.Comment;
import models.Post;
import models.User;
import play.Play;
import play.cache.Cache;
import play.data.validation.Required;
import play.db.DB;
import play.libs.Codec;
import play.libs.Images;
import play.mvc.Before;
import play.mvc.Controller;

public class Application extends Controller
{

    @Before
    static void addDefaults()
    {
        renderArgs.put("blogTitle", Play.configuration.getProperty("blog.title"));
        renderArgs.put("blogBaseline", Play.configuration.getProperty("blog.baseline"));
    }

    public static void index()
    {
        Post frontPost = Post.find("order by postedAt desc").first();
        List<Post> olderPosts = Post.find("order by postedAt desc").from(1).fetch(10);
        render(frontPost, olderPosts);
    }

    public static void show(Long id)
    {
        Post post = Post.findById(id);
        String randomID = Codec.UUID();
        render(post, randomID);
    }

    public static void captcha(String id)
    {
        Images.Captcha captcha = Images.captcha();
        String code = captcha.getText("#E4EAFD");
        Cache.set(id, code, "10mn");
        renderBinary(captcha);
    }

    public static void postComment(Long postId, @Required(message = "Author is required") String author, @Required(message = "A message is required") String content,
            @Required(message = "Please type the code") String code, String randomID)
    {
        Post post = Post.findById(postId);
        validation.equals(code, Cache.get(randomID)).message("Invalid code. Please type it again");
        if (validation.hasErrors())
        {
            render("Application/show.html", post, randomID);
        }
        post.addComment(author, content);
        flash.success("Thanks for posting %s", author);
        Cache.delete(randomID);
        show(postId);
    }

    public static void listTagged(String tag)
    {
        List<Post> posts = Post.findTaggedWith(tag);
        render(tag, posts);
    }

    public static void hello()
    {
        System.err.println("im heere");
        try
        {
            Connection conn = DB.getConnection();
            PreparedStatement stmt = conn.prepareStatement("select * from example.note");
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
            {
                String id = rs.getString("ID");
                String desc = rs.getString("DESCRIPTION");
                System.err.println("ID " + id);
                System.err.println("DESCRIPTION : " + desc);
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        User bob = new User("bob@gmail.com", "secret", "Bob").save();
        System.err.println("bob id " + bob.getId());
        Post post = new Post(bob, "Title 465465464", "Content of the blog post");
        post.tagItWith("play");
        post.tagItWith("test");
        post.tagItWith("hibernate");
        post.save();
        new Comment(post, "Marek", "Comentujem nieco").save();
        render();
    }

}